<div class="dropdown">
  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
  <span class="caret"></span></button>
  <ul class="dropdown-menu dropdown-menu-right">
    <li><a href="{{ route('admin.servers.member', $member->id) }}"><i class='fa fa-user'></i>  Servers</a></li>
    <li><a href="{{ route('admin.roles.member', $member->id) }}"><i class='fa fa-users'></i>  Roles</a></li>
  </ul>
</div>
