<div class="dropdown">
  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
  <span class="caret"></span></button>
  <ul class="dropdown-menu dropdown-menu-right">
    <li><a href="{{ route('admin.members.role', $role->id) }}"><i class='fa fa-user'></i>  Members</a></li>
  </ul>
</div>
