@extends('la.layouts.app')

@section('htmlheader_title') Members @endsection
@section('contentheader_title') Members @endsection
@section('contentheader_description') Overview of member: {{ $member->name }} @endsection

@section('main-content')
<section class="content">
  <div class="row">
    <section class="col-lg-12">
      <div class="nav-tabs-custom">
        <table class="table table-striped">
          <tbody>
            <tr><td><img src="{{ $member->getAvatar() }}" class="img-circle server-icon" alt="Member Image" /></td>
              <td><div class="dropdown">
                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
                <span class="caret"></span></button>
                <ul class="dropdown-menu">
                  <li><a href="{{ route('admin.servers.member', $member->id) }}"><i class='fa fa-user'></i>  Servers</a></li>
                  <li><a href="{{ route('admin.roles.member', $member->id) }}"><i class='fa fa-users'></i>  Roles</a></li>
                </ul>
              </div></td></tr>
            <tr><th>Name</th><td> {{ $member->name }} </td></tr>
            <tr><th>Discriminator</th><td> {{ $member->discriminator }} </td></tr>
            <tr><th>Bot</th><td> {{ $member->bot }} </td></tr>
            <tr><th>Created_at</th><td> {{ $member->created_at }} </td></tr>
          </tbody>
        </table>
      </div>
    </section>
  </div>
</section>
@endsection

@push('styles')
  <link rel="stylesheet" href="{{ asset('dlb-assets/css/style.css') }}">
@endpush


@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('la-assets/plugins/knob/jquery.knob.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>
@endpush

@push('scripts')
<script>
</script>
@endpush
