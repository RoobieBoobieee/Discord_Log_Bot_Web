@extends('la.layouts.app')

@section('htmlheader_title') Servers @endsection
@section('contentheader_title') Servers ({{ $emojis->total() }}) @endsection
@section('contentheader_description') Overview of servers @endsection

@section('main-content')
<section class="content">
  <div class="row">
    <section class="col-lg-12">
      <div class="nav-tabs-custom">
        <table class="table table-striped">
          <thead>
            <tr>
              <th></th>
              <th>Name</th>
              <th data-toggle="tooltip" data-placement="bottom" title="If colons are required to use this emoji in the client (:PJSalt: vs PJSalt).">Require Colons</th>
              <th data-toggle="tooltip" data-placement="bottom" title="If this emoji is managed by a Twitch integration.">managed</th>
              <th>Server</th>
              <th>Created At</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($emojis as $emoji)
              <tr>
                <td> <img src="{{ $emoji->url }}" class="discord_emoji" alt="Emoji" /></td>
                <td> {{ $emoji->name }} </td>
                <td> {{ $emoji->require_colons }} </td>
                <td> {{ $emoji->managed }} </td>
                <td> <a href="{{ route('admin.servers.server', $emoji->server) }}">{{ App\Server::find($emoji->server)->name }}</a> </td>
                <td> {{ $emoji->created_at }} </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      {{ $emojis->links() }}
    </section>
  </div>
</section>
@endsection

@push('styles')
  <link rel="stylesheet" href="{{ asset('dlb-assets/css/style.css') }}">
@endpush


@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('la-assets/plugins/knob/jquery.knob.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>
@endpush

@push('scripts')
<script>
</script>
@endpush
