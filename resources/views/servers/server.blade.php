@extends('la.layouts.app')

@section('htmlheader_title') Servers @endsection
@section('contentheader_title') Server @endsection
@section('contentheader_description') Overview of server: {{ $server->name }} @endsection

@section('main-content')
<section class="content">
  <div class="row">
    <section class="col-lg-12">
      <div class="nav-tabs-custom">
        <table class="table table-striped">
          <tbody>
            <tr><td><img src="{{ $server->getIcon() }}" class="img-circle server-icon" alt="Server Image" /></td><td>
              <div class="dropdown">
                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
                <span class="caret"></span></button>
                <ul class="dropdown-menu">
                  <li><a href="{{ route('admin.members.server', $server->id) }}"><i class='fa fa-user'></i>  Members</a></li>
                  <li><a href="{{ route('admin.channels.server', $server->id) }}"><i class='fa fa-list'></i>  Channels</a></li>
                </ul>
              </div></td></tr>
            <tr><th>Name</th><td> {{ $server->name }} </td></tr>
            <tr><th>Member Count</th><td> {{ $server->member_count }} </td></tr>
            <tr><th>Region</th><td> {{ $server->region }} </td></tr>
            <tr><th>Owner</th><td>  <a href="{{ route('admin.members.member', $server->owner) }}">{{ App\Member::find($server->owner)->name }}</a></td></tr>
            <tr><th>MFA Level</th><td> {{ $server->mfa_level }} </td></tr>
            <tr><th>Verification Level</th><td> {{ $server->verification_level }} </td></tr>
            <tr><th>Default Channel</th><td><a href="{{ route('admin.channels.channel', $server->default_channel) }}">{{ App\Channel::find($server->default_channel)->name }}</a> </td></tr>
            <tr><th>AFK Channel</th><td>
              @if ($afk_channel = App\Channel::find($server->afk_channel))
                <a href="{{ route('admin.channels.channel', $server->afk_channel) }}">{{ $afk_channel->name }}</a>
              @endif
            </td></tr>
            <tr><th>AFK Timeout</th><td> {{ $server->afk_timeout/60 }}m </td></tr>
            <tr><th>Created At</th><td> {{ $server->created_at }} </td></tr>
          </tbody>
        </table>
      </div>
    </section>
  </div>
</section>
@endsection

@push('styles')
  <link rel="stylesheet" href="{{ asset('dlb-assets/css/style.css') }}">
@endpush


@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('la-assets/plugins/knob/jquery.knob.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>
@endpush

@push('scripts')
<script>
</script>
@endpush
