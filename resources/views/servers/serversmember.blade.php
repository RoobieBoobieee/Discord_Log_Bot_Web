@extends('la.layouts.app')

@section('htmlheader_title') Servers @endsection
@section('contentheader_title') Servers ({{ $servers->count() }}) @endsection
@section('contentheader_description') Overview of servers for member:  {{ App\Member::where('id', $member_id)->first()->name }} @endsection

@section('main-content')
<section class="content">
  <div class="row">
    <section class="col-lg-12">
      <div class="nav-tabs-custom">
        <table class="table table-striped">
          <thead>
            <tr>
              <th></th>
              <th>Server Name</th>
              <th>Member</th>
              <th>Nickname</th>
              <th># Members</th>
              <th>Region</th>
              <th>Owner</th>
              <th data-toggle="tooltip" data-placement="bottom" title="Indicates the server’s two factor authorisation level. If this is set then the server requires 2FA for their administrative members.">MFA</th>
              <th data-toggle="tooltip" data-placement="bottom" title="Specifies a Server‘s verification level.
              none: No criteria set.
              low: Member must have a verified email.
              medium: Member must have a verified email and be registered for more than five minutes.
              high: Member must have a verified email, be registered for more than five minutes, and be a member of the server itself for more than ten minutes.">Verification</th>
              <th>Default Channel</th>
              <th>AFK Channel</th>
              <th>AFK Timeout</th>
              <th>Created At</th>
              <th>Joined At</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($servers as $server)
              <tr>
                <td> <img src="{{ $server->getIcon() }}" class="img-circle server-icon" alt="Server Image" /></td>
                <td>  <a href="{{ route('admin.servers.server', $server->id) }}">{{ $server->name }}</a>  </td>
                <td> <a href="{{ route('admin.members.member', $member_id) }}"> {{ App\Member::where('id', $member_id)->first()->name }}</a> </td>
                <td> {{ $server->members()->where('id', $member_id)->first()->pivot->display_name }} </td>
                <td> {{ $server->member_count }} </td>
                <td> {{ $server->region }} </td>
                <td> <a href="{{ route('admin.members.member', $server->owner) }}">{{ App\Member::find($server->owner)->name }}</a> </td>
                <td>
                  @if ($server->mfa_level == 0)
                    no
                  @else
                    yes
                  @endif
                </td>
                <td> {{ $server->verification_level }} </td>
                <td>  <a href="{{ route('admin.channels.channel', $server->default_channel) }}">{{ App\Channel::find($server->default_channel)->name }}</a> </td>
                <td>
                    @if ($afk_channel = App\Channel::find($server->afk_channel))
                      <a href="{{ route('admin.channels.channel', $server->afk_channel) }}">{{ $afk_channel->name }}</a>
                    @endif
                </td>
                <td> {{ $server->afk_timeout/60 }}m </td>
                <td> {{ $server->created_at }} </td>
                <td> {{ $server->members()->where('id', $member_id)->first()->pivot->joined_at }} </td>
                <td>@include('actions.server')</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </section>
  </div>
</section>
@endsection

@push('styles')
  <link rel="stylesheet" href="{{ asset('dlb-assets/css/style.css') }}">
@endpush


@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('la-assets/plugins/knob/jquery.knob.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>
@endpush

@push('scripts')
<script>
</script>
@endpush
