@extends('la.layouts.app')

@section('htmlheader_title') Channels @endsection
@section('contentheader_title') Channels ({{ $channels->count() }}) @endsection
@section('contentheader_description') Overview of channels for server: {{ App\Server::where('id', $server_id)->first()->name }} @endsection

@section('main-content')
<section class="content">
  <div class="row">
    <section class="col-lg-12">
      <div class="nav-tabs-custom">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Server</th>
              <th>Type</th>
              <th>Position</th>
              <th>Bitrate</th>
              <th>User Limit</th>
              <th>Default</th>
              <th>Created At</th>
              <th>Topic</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($channels as $channel)
              <tr>
                <td> <a href="{{ route('admin.channels.channel', $channel->id) }}">{{ $channel->name }}</a> </td>
                <td> <a href="{{ route('admin.servers.server', $channel->server) }}">{{ App\Server::find($channel->server)->name }}</a> </td>
                <td> {{ $channel->type }} </td>
                <td> {{ $channel->position }} </td>
                <td> {{ $channel->bitrate }} </td>
                <td>
                  @if ($channel->user_limit == 0)
                    none
                  @else
                    {{ $channel->user_limit }}
                  @endif
                </td>
                <td>
                  @if ($channel->is_default == 0)
                    no
                  @else
                    yes
                  @endif
                </td>
                <td> {{ $channel->created_at }} </td>
                <td> {{ $channel->topic }} </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </section>
  </div>
</section>
@endsection

@push('styles')
  <link rel="stylesheet" href="{{ asset('dlb-assets/css/style.css') }}">
@endpush


@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('la-assets/plugins/knob/jquery.knob.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>
@endpush

@push('scripts')
<script>
</script>
@endpush
