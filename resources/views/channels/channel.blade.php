@extends('la.layouts.app')

@section('htmlheader_title') Channel @endsection
@section('contentheader_title') Channel @endsection
@section('contentheader_description') Overview of {{ $channel->name }} @endsection

@section('main-content')
<section class="content">
  <div class="row">
    <section class="col-lg-12">
      <div class="nav-tabs-custom">
        <table class="table table-striped">
          <tbody>
                <tr><th>Name</th><td><a href="{{ route('admin.channels.channel', $channel->id) }}">{{ $channel->name }}</a> </td></tr>
                <tr><th>Server</th><td> <a href="{{ route('admin.servers.server', $channel->server) }}">{{ App\Server::find($channel->server)->name }}</a> </td></tr>
                <tr><th>Type</th><td> {{ $channel->type }} </td></tr>
                <tr><th>Topic</th><td> {{ $channel->topic }} </td></tr>
                <tr><th>Private</th><td>
                  @if ($channel->is_private == 0)
                    no
                  @else
                    ye
                  @endif</td></tr>
                <tr><th>Position</th><td> {{ $channel->position }} </td></tr>
                <tr><th>Bitrate</th><td> {{ $channel->bitrate }} </td></tr>
                <tr><th>User Limit</th><td>
                  @if ($channel->user_limit == 0)
                    none
                  @else
                    {{ $channel->user_limit }}
                  @endif </td></tr>
                <tr><th>Default</th><td> {{ $channel->is_default }} </td></tr>
                <tr><th>Created At</th><td> {{ $channel->created_at }} </td></tr>
                <tr><th>Deleted At</th><td> {{ $channel->deleted }} </td></tr>
          </tbody>
        </table>
      </div>
    </section>
  </div>
</section>
@endsection

@push('styles')
  <link rel="stylesheet" href="{{ asset('dlb-assets/css/style.css') }}">
@endpush


@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('la-assets/plugins/knob/jquery.knob.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>
@endpush

@push('scripts')
<script>
</script>
@endpush
