@extends('la.layouts.app')

@section('htmlheader_title') Channels @endsection
@section('contentheader_title') Channel Activity @endsection
@section('contentheader_description') Overview of channels @endsection

@section('main-content')
<section class="content">
  <div class="row">
    <section class="col-lg-12">
      <div class="nav-tabs-custom">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Timestamp</th>
              <th>User</th>
              <th>Description</th>
          </thead>
          <tbody>
            @foreach ($activities as $activity)
              <tr>
                <td>{{ $activity->timestamp }}</td>
                <td><a href="{{ route('admin.members.member', $activity->member_id) }}">{{ App\Member::find($activity->member_id)->name }}</a></td>
                <td>
                  @if ($activity->attribute === "voice_channel")
                    @if ($activity->before === "None")
                      joined <a href="{{ route('admin.servers.server', $activity->after) }}">{{ App\Channel::find($activity->after)->name }}</a>
                    @elseif ($activity->after === "None")
                      left <a href="{{ route('admin.servers.server', $activity->before) }}">{{ App\Channel::find($activity->before)->name }}</a>
                    @else
                      switched from
                      <a href="{{ route('admin.servers.server', $activity->before) }}">{{ App\Channel::find($activity->before)->name }}</a>
                      to
                      <a href="{{ route('admin.servers.server', $activity->after) }}">{{ App\Channel::find($activity->after)->name }}</a>
                    @endif

                  @elseif ($activity->attribute === "self_mute")
                    @if ($activity->before === "False")
                      muted him-, her- or itself
                    @elseif ($activity->after === "False")
                      unmuted him-, her- or itself
                    @endif

                  @elseif ($activity->attribute === "self_deaf")
                    @if ($activity->before === "False")
                      deafened him-, her- or itself
                    @elseif ($activity->after === "False")
                      undeafened him-, her- or itself
                    @endif

                  @elseif ($activity->attribute === "server_mute")
                    @if ($activity->before === "False")
                      got a server mute.
                    @elseif ($activity->after === "False")
                      lost the server mute.
                    @endif

                  @elseif ($activity->attribute === "server_deaf")
                    @if ($activity->before === "False")
                      got a server deaf.
                    @elseif ($activity->after === "False")
                      lost the server deaf.
                    @endif

                  @elseif ($activity->attribute == "is_afk")
                    @if ($activity->before == "False")
                      went afk.
                    @elseif ($activity->after == "False")
                      is back.
                    @endif
                  @else
                  {{$activity->attribute}}

                @endif
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>

      {{ $activities->links() }}
    </section>
  </div>
</section>
@endsection

@push('styles')
  <link rel="stylesheet" href="{{ asset('dlb-assets/css/style.css') }}">
@endpush


@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('la-assets/plugins/knob/jquery.knob.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>
@endpush

@push('scripts')
<script>
</script>
@endpush
