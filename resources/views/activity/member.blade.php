@extends('la.layouts.app')

@section('htmlheader_title') Channels @endsection
@section('contentheader_title') Channel Activity @endsection
@section('contentheader_description') Overview of channels @endsection

@section('main-content')
<section class="content">
  <div class="row">
    <section class="col-lg-12">
      <div class="nav-tabs-custom">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Timestamp</th>
              <th>Server</th>
              <th>User</th>
              <th>Description</th>
          </thead>
          <tbody>
            @foreach ($activities as $activity)
              <tr>
                <td>{{ $activity->timestamp }}</td>
                <td><a href="{{ route('admin.servers.server', $activity->server_id) }}">{{ App\Server::find($activity->server_id)->name }}</a></td>
                <td>
                  @if ($member = App\Member::find($activity->member_id))
                    <a href="{{ route('admin.members.member', $activity->member_id) }}">{{ $member->name }}</a>
                  @else
                    Member not found.
                  @endif
                </td>
                <td>
                  @if ($activity->attribute === "status")
                      switched from {{ $activity->before }} to {{ $activity->after }}
                  @elseif ($activity->attribute === "game")
                    @if ($activity->before === "None")
                      started playing {{ $activity->after }}
                    @elseif ($activity->after === "None")
                      stoped playing {{ $activity->before }}
                    @else
                      switched from playing {{ $activity->before }} to {{ $activity->after }}
                    @endif
                  @elseif ($activity->attribute === "avatar")
                    @if ($activity->before === "")
                      added an avatar <img src="{{ $activity->after }}" class="img-circle server-icon" alt="avatar" />
                    @elseif ($activity->after === "")
                      removed an avatar <img src="{{ $activity->before }}" class="img-circle server-icon" alt="avatar" />
                    @else
                      switched avatars. From: <img src="{{ $activity->before }}" class="img-circle server-icon" alt="avatar" />
                      to <img src="{{ $activity->after }}" class="img-circle server-icon" alt="avatar" />
                    @endif
                  @elseif ($activity->attribute === "left")
                    left the server.
                  @elseif ($activity->attribute === "joined")
                    joined the server.
                  @elseif ($activity->attribute === "role")
                    @if ($activity->after === "removed")
                      lost role: {{ App\Discord_Role::find($activity->before)->name }}
                    @elseif ($activity->after === "added")
                      got role: {{ App\Discord_Role::find($activity->before)->name }}
                    @endif
                  @elseif ($activity->attribute === "nick")
                    switched nickname from {{ $activity->before }} to  {{ $activity->after }}.
                  @else
                  {{$activity->attribute}}

                @endif
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>

      {{ $activities->links() }}
    </section>
  </div>
</section>
@endsection

@push('styles')
  <link rel="stylesheet" href="{{ asset('dlb-assets/css/style.css') }}">
@endpush


@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('la-assets/plugins/knob/jquery.knob.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>
@endpush

@push('scripts')
<script>
</script>
@endpush
