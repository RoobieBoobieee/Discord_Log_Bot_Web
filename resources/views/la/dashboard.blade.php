@extends('la.layouts.app')

@section('htmlheader_title') Dashboard @endsection
@section('contentheader_title') Dashboard @endsection
@section('contentheader_description') Overview of logged data @endsection

@section('main-content')
<!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <a href="{{ route('admin.servers') }}">
                <div class="small-box bg-aqua">
                  <div class="inner">
                    <h3>{{ $counts['servers'] }}</h3>
                    <p>Total Servers</p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-server"></i>
                  </div>
                </div>
              </a>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <a href="{{ route('admin.channels') }}">
                <div class="small-box bg-green">
                  <div class="inner">
                    <h3>{{ $counts['channels'] }}</h3>
                    <p>Total Channels</p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-list"></i>
                  </div>
                </div>
              </a>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <a href="{{ route('admin.members') }}">
                <div class="small-box bg-yellow">
                  <div class="inner">
                    <h3>{{ $counts['members'] }}</h3>
                    <p>Total Users</p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-user"></i>
                  </div>
                </div>
              </a>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3>{{ $counts['messages']['total'] }}</h3>
                  <p>Total Messages</p>
                </div>
                <div class="icon">
                  <i class="fa fa-comment"></i>
                </div>
              </div>
            </div><!-- ./col -->
          </div><!-- /.row -->
          <!-- Main row -->
          <div class="row">

            <section class="col-lg-12">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs pull-right">
                <li class="pull-left header"><i class="fa fa-inbox"></i> Messages / Voice Connections (Last Two Weeks)</li>
              </ul>
              <div class="tab-content no-padding">
                <!-- Morris chart - Sales -->
                <div class="chart tab-pane active" id="data-chart" style="position: relative; height: 500px;"></div>
              </div>
            </div><!-- /.nav-tabs-custom -->
          </section>
        </section>

@endsection

@push('styles')
<!-- Morris chart -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/morris/morris.css') }}">
<link rel="stylesheet" href="{{ asset('dlb-assets/css/style.css') }}">
@endpush

@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{ asset('la-assets/plugins/morris/morris.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>
@endpush

@push('scripts')
<script>
//(function($) {
// 	$('body').pgNotification({
// 		style: 'circle',
// 		title: 'LaraAdmin',
// 		message: "Welcome to LaraAdmin...",
// 		position: "top-right",
// 		timeout: 0,
// 		type: "success",
// 		thumbnail: '<img width="40" height="40" style="display: inline-block;" src="{{ Gravatar::fallback(asset('la-assets/img/user2-160x160.jpg'))->get(Auth::user()->email, 'default') }}" data-src="assets/img/profiles/avatar.jpg" data-src-retina="assets/img/profiles/avatar2x.jpg" alt="">'
// 	}).show();
//})(window.jQuery);
function voice_messages_chart_addData(voice_messages_chart) {
  voice_messages_chart.setData([
    @foreach ($counts['messages'] as $key => $value)
      @if ($key != 'total')
        {x: '{{ $key }}', messages:  {{ $value }}, voiceconnections:  {{ $counts['voiceconnections'][$key] }} },
      @endif
    @endforeach
  ]);
};
</script>
@endpush
