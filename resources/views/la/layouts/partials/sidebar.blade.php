<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ Gravatar::fallback(asset('la-assets/img/user2-160x160.jpg'))->get(Auth::user()->email) }}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
        @endif

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">MENU</li>
            <!-- Optionally, you can add icons to the links -->
            <li><a href="{{ url(config('laraadmin.adminRoute')) }}"><i class='fa fa-home'></i> <span>Dashboard</span></a></li>
            <li><a href="{{ url(config('laraadmin.adminRoute') . '/servers') }}"><i class='fa fa-server'></i> <span>Servers</span></a></li>
            <li  data-toggle="collapse" data-target="#channels" class="collapsed">
                  <a href="#"><i class='fa fa-list'></i> <span>Channels</span></a>
            </li>
            <ul class="sub-menu" id="channels">
                <li><a href="{{ route('admin.channels') }}">- <span>All</span></a></li>
                <li><a href="{{ route('admin.channels.voice') }}">- <span>Voice</span></a></li>
                <li><a href="{{ route('admin.channels.text') }}">- <span>Text</span></a></li>
                <li><a href="{{ route('admin.privatechannels') }}">- <span>Private</span></a></li>
            </ul>
            <li><a href="{{ url(config('laraadmin.adminRoute') . '/members') }}"><i class='fa fa-user'></i> <span>Members</span></a></li>
            <ul class="sub-menu" id="channels">
                <li><a href="{{ route('admin.members.bots') }}">- <span>Bots</span></a></li>
            </ul>
            <li><a href="{{ url(config('laraadmin.adminRoute') . '/roles') }}"><i class='fa fa-users'></i> <span>Roles</span></a></li>
            <li><a href="{{ route('admin.invites') }}"><i class='fa fa-envelope-open-o'></i> <span>Invites</span></a></li>
            <li><a href="{{ route('admin.bans') }}"><i class='fa fa-ban'></i> <span>Bans</span></a></li>
            <li><a href="{{ route('admin.emojis') }}"><i class='fa fa-smile-o'></i> <span>Emojis</span></a></li>

            <li  data-toggle="collapse" data-target="#activity" class="collapsed">
                  <a href="#"><i class='fa fa-line-chart'></i> <span>Activity</span></a>
            </li>
            <ul class="sub-menu" id="activity">
                <li><a href="{{ route('admin.activity.voice') }}">- <span>Voice</span></a></li>
                <li><a href="{{ route('admin.activity.server') }}">- <span>Server</span></a></li>
                <li><a href="{{ route('admin.activity.member') }}">- <span>Member</span></a></li>
                <li><a href="{{ route('admin.activity.channel') }}">- <span>Channel</span></a></li>
            </ul>


        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
