@if(!isset($no_padding))
<footer class="main-footer">
    <strong>Copyright &copy; 2016 - {{ date("Y") }}</strong> - Rob Van Keilegom - <a target="_blank" href="https://www.robvankeilegom.be">www.robvankeilegom.be</a>
</footer>
@endif
