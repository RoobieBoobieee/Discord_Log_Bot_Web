<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
  protected $connection = 'discord_log_db';
  protected $table = 'channels';
  protected $primaryKey = 'id'; // unnecessary, just to be sure

  public $incrementing = false;
  public $timestamps = false;

  protected $fillable = array('name', 'type', 'topic', 'is_private', 'position', 'bitrate', 'user_limit', 'is_default');
  protected $dates = ['created_at', 'deleted'];

  public function server() {
    return $this->hasOne('App\Server', 'id', 'server');
  }

}
