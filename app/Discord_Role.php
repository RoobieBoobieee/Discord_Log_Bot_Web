<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discord_Role extends Model
{
  protected $connection = 'discord_log_db';
  protected $table = 'roles';
  protected $primaryKey = 'id'; // unnecessary, just to be sure

  public $incrementing = false;
  public $timestamps = false;

  protected $fillable = array('name', 'color', 'hoist', 'position', 'managed', 'mentionable');
  protected $dates = ['created_at', 'deleted'];

  public function server() {
    return $this->hasOne('App\Server', 'id', 'server');
  }

  public function members()
  {
      return $this->belongsToMany('App\Member', 'member_roles', 'role_id', 'member_id');
  }

  public function permissions() {
    return $this->hasOne('App\Permission', 'permissions', 'value');
  }

  public function color() {
    return("#".substr("000000".dechex($this->color), -6));
  }
}
