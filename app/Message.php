<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Member;
use App\Channel;

class Message extends Model
{
  protected $connection = 'discord_log_db';
  protected $table = 'messages';
  protected $primaryKey = 'id'; // unnecessary, just to be sure

  public $incrementing = false;
  public $timestamps = false;

  protected $fillable = array('content', 'tts', 'type', 'mention_everyone', 'version', 'deleted');
  protected $dates = ['timestamp', 'deleted'];

  public function author() {
      return $this->hasOne('Member');
  }

  public function channel() {
        return $this->hasOne('Channel');
  }

  public function server() {
        return $this->hasOne('Server');
  }

}
