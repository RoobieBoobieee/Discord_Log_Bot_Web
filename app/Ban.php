<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ban extends Model
{
  protected $connection = 'discord_log_db';
  protected $table = 'bans';
  protected $primaryKey = ['server_id', 'member_id'];

  public $incrementing = false;
  public $timestamps = false;


  public function server() {
    return $this->hasOne('App\Server', 'id', 'server_id');
  }

  public function member() {
    return $this->hasOne('App\Member', 'id', 'member_id');
  }

}
