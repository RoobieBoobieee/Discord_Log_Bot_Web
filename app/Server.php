<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
  protected $connection = 'discord_log_db';
  protected $table = 'servers';
  protected $primaryKey = 'id'; // unnecessary, just to be sure

  public $incrementing = false;
  public $timestamps = false;

  protected $fillable = array('name', 'region', 'afk_timeout', 'icon', 'mfa_level', 'verification_level', 'splash', 'icon', 'icon_url', 'splash_url', 'member_count');
  protected $dates = ['created_at', 'deleted'];

  public function owner()
  {
    return $this->hasOne('Member');
  }

  public function afk_channel()
  {
    return $this->hasOne('Channel');
  }

  public function default_channel()
  {
    return $this->hasOne('Channel');
  }

  public function channels()
  {
      return $this->hasMany('App\Channel', 'server', 'id');
  }

  public function members()
  {
      return $this->belongsToMany('App\Member', 'servers_members', 'server_id', 'member_id')->withPivot('joined_at', 'display_name');
  }

  public function roles()
  {
      return $this->hasMany('App\Discord_Role', 'server', 'id');
  }

  public function getIcon()
  {
    if ($this->icon_url != "")
      return $this->icon_url;
    return asset('dlb-assets/img/default.jpg');
  }
}
