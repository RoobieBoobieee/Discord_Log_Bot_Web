<?php

/* ================== Homepage ================== */
Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::auth();

/* ================== Access Uploaded Files ================== */
//Route::get('files/{hash}/{name}', 'LA\UploadsController@get_file');

/*
|--------------------------------------------------------------------------
| Admin Application Routes
|--------------------------------------------------------------------------
*/

$as = "";
if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
	$as = config('laraadmin.adminRoute').'.';

	// Routes for Laravel 5.3
	Route::get('/logout', 'Auth\LoginController@logout');
}

Route::group(array('prefix' => config('laraadmin.adminRoute'), 'as' => $as, 'middleware' => ['auth', 'permission:ADMIN_PANEL']), function () {

	/* ================== Dashboard ================== */

	Route::get('/', 'LA\DashboardController@index');

	Route::group(['as' => 'admin.', 'prefix' => 'servers'], function () {
		Route::get('/', ['as' => 'servers', 'uses' =>'LA\DashboardController@servers']);
		Route::get('/server/{id}', ['as' => 'servers.server', 'uses' =>'LA\DashboardController@servers_server']);
		Route::get('/member/{id}', ['as' => 'servers.member', 'uses' =>'LA\DashboardController@servers_member']);
	});

	Route::group(['as' => 'admin.', 'prefix' => 'channels'], function () {
		Route::get('/', ['as' => 'channels', 'uses' =>'LA\DashboardController@channels']);
		Route::get('/channel/{id}', ['as' => 'channels.channel', 'uses' =>'LA\DashboardController@channels_channel']);
		Route::get('/voice', ['as' => 'channels.voice', 'uses' =>'LA\DashboardController@voicechannels']);
		Route::get('/text', ['as' => 'channels.text', 'uses' =>'LA\DashboardController@textchannels']);
		Route::get('/server/{id}', ['as' => 'channels.server', 'uses' =>'LA\DashboardController@channels_server']);
	});

	Route::group(['as' => 'admin.', 'prefix' => 'privatechannels'], function () {
		Route::get('/', ['as' => 'privatechannels', 'uses' =>'LA\DashboardController@privatechannels']);
	});

	Route::group(['as' => 'admin.', 'prefix' => 'members'], function () {
		Route::get('/', ['as' => 'members', 'uses' =>'LA\DashboardController@members']);
		Route::get('/bots/', ['as' => 'members.bots', 'uses' =>'LA\DashboardController@members_bots']);
		Route::get('/member/{id}', ['as' => 'members.member', 'uses' =>'LA\DashboardController@members_member']);
		Route::get('/server/{id}', ['as' => 'members.server', 'uses' =>'LA\DashboardController@members_server']);
		Route::get('/privatechannel/{id}', ['as' => 'members.channel', 'uses' =>'LA\DashboardController@members_channel']);
		Route::get('/role/{id}', ['as' => 'members.role', 'uses' =>'LA\DashboardController@members_role']);
	});

	Route::group(['as' => 'admin.', 'prefix' => 'roles'], function () {
		Route::get('/', ['as' => 'roles', 'uses' =>'LA\DashboardController@roles']);
		Route::get('/role/{id}', ['as' => 'roles.role', 'uses' =>'LA\DashboardController@roles_role']);
		Route::get('/server/{id}', ['as' => 'roles.server', 'uses' =>'LA\DashboardController@roles_server']);
		Route::get('/member/{id}', ['as' => 'roles.member', 'uses' =>'LA\DashboardController@roles_member']);
	});

	Route::group(['as' => 'admin.', 'prefix' => 'invites'], function () {
		Route::get('/', ['as' => 'invites', 'uses' =>'LA\DashboardController@invites']);
	});

	Route::group(['as' => 'admin.', 'prefix' => 'activity'], function () {
		Route::get('/voice', ['as' => 'activity.voice', 'uses' =>'LA\DashboardController@voice_activity']);
		Route::get('/server', ['as' => 'activity.server', 'uses' =>'LA\DashboardController@server_activity']);
		Route::get('/member', ['as' => 'activity.member', 'uses' =>'LA\DashboardController@member_activity']);
		Route::get('/channel', ['as' => 'activity.channel', 'uses' =>'LA\DashboardController@channel_activity']);
	});

	Route::group(['as' => 'admin.', 'prefix' => 'emojis'], function () {
		Route::get('/', ['as' => 'emojis', 'uses' =>'LA\DashboardController@emojis']);
	});

	Route::group(['as' => 'admin.', 'prefix' => 'bans'], function () {
		Route::get('/', ['as' => 'bans', 'uses' =>'LA\DashboardController@bans']);
	});

	// /* ================== Users ================== */
	// Route::resource(config('laraadmin.adminRoute') . '/users', 'LA\UsersController');
	// Route::get(config('laraadmin.adminRoute') . '/user_dt_ajax', 'LA\UsersController@dtajax');
	//
	// /* ================== Uploads ================== */
	// Route::resource(config('laraadmin.adminRoute') . '/uploads', 'LA\UploadsController');
	// Route::post(config('laraadmin.adminRoute') . '/upload_files', 'LA\UploadsController@upload_files');
	// Route::get(config('laraadmin.adminRoute') . '/uploaded_files', 'LA\UploadsController@uploaded_files');
	// Route::post(config('laraadmin.adminRoute') . '/uploads_update_caption', 'LA\UploadsController@update_caption');
	// Route::post(config('laraadmin.adminRoute') . '/uploads_update_filename', 'LA\UploadsController@update_filename');
	// Route::post(config('laraadmin.adminRoute') . '/uploads_update_public', 'LA\UploadsController@update_public');
	// Route::post(config('laraadmin.adminRoute') . '/uploads_delete_file', 'LA\UploadsController@delete_file');
	//
	// /* ================== Roles ================== */
	// Route::resource(config('laraadmin.adminRoute') . '/roles', 'LA\RolesController');
	// Route::get(config('laraadmin.adminRoute') . '/role_dt_ajax', 'LA\RolesController@dtajax');
	// Route::post(config('laraadmin.adminRoute') . '/save_module_role_permissions/{id}', 'LA\RolesController@save_module_role_permissions');
	//
	// /* ================== Permissions ================== */
	// Route::resource(config('laraadmin.adminRoute') . '/permissions', 'LA\PermissionsController');
	// Route::get(config('laraadmin.adminRoute') . '/permission_dt_ajax', 'LA\PermissionsController@dtajax');
	// Route::post(config('laraadmin.adminRoute') . '/save_permissions/{id}', 'LA\PermissionsController@save_permissions');
	//
	// /* ================== Departments ================== */
	// Route::resource(config('laraadmin.adminRoute') . '/departments', 'LA\DepartmentsController');
	// Route::get(config('laraadmin.adminRoute') . '/department_dt_ajax', 'LA\DepartmentsController@dtajax');
	//
	// /* ================== Employees ================== */
	// Route::resource(config('laraadmin.adminRoute') . '/employees', 'LA\EmployeesController');
	// Route::get(config('laraadmin.adminRoute') . '/employee_dt_ajax', 'LA\EmployeesController@dtajax');
	// Route::post(config('laraadmin.adminRoute') . '/change_password/{id}', 'LA\EmployeesController@change_password');
	//
	// /* ================== Organizations ================== */
	// Route::resource(config('laraadmin.adminRoute') . '/organizations', 'LA\OrganizationsController');
	// Route::get(config('laraadmin.adminRoute') . '/organization_dt_ajax', 'LA\OrganizationsController@dtajax');
	//
	// /* ================== Backups ================== */
	// Route::resource(config('laraadmin.adminRoute') . '/backups', 'LA\BackupsController');
	// Route::get(config('laraadmin.adminRoute') . '/backup_dt_ajax', 'LA\BackupsController@dtajax');
	// Route::post(config('laraadmin.adminRoute') . '/create_backup_ajax', 'LA\BackupsController@create_backup_ajax');
	// Route::get(config('laraadmin.adminRoute') . '/downloadBackup/{id}', 'LA\BackupsController@downloadBackup');
});
