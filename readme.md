# ROBot.py Web interface
![Screenshot](http://i.imgur.com/KlYGFMw.jpg "Admin Dashboard")

This is a WIP.


#### INSTALL
You should have a webserver and composer installed.

```
$ cd Discord_Log_Bot_Web

$ cp .env.example .env

```

Edit the `.env` file. Add as `DB_CONNECTION` your credentials and database name for the web interface database. As `LOG_DB_CONNECTION` you enter the same credentials you used for your bot (these can be the same, just a different database).

After that go back to your command line and enter:

```
$ composer install

$ php artisan key:generate

$ php artisan migrate

$ php artisan db:seed

```

Then surf to your IP in the browser and follow the instructions to make an admin account.
